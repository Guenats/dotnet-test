FROM bash:4.3
COPY . /app
ENV URL "127.0.0.1"
RUN mkdir /bash
ADD /PingCount/PingCount.sh /PingCount.sh
WORKDIR /
RUN chmod +x PingCount.sh
RUN ls
RUN apk update
RUN apk add jq
RUN apk add curl
RUN apk add bc

CMD ["bash", "./PingCount.sh"] 
