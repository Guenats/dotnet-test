﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingCount.Models
{
    public class Ping
    {
        public long Id { get; set; }

        public int pingCount { get; set; }
   
	public string message { get; set; }
     }
}
